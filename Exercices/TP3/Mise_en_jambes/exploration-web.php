<?php

require_once("../Class/Movie.php");

// On stock par défaut null
$movie = null;

// On vérifie que les valeurs nécessaires sont contenu dans le tableau $_GET 
if(isset($_GET["id_movie"]) && !empty($_GET["id_movie"])) {
    
    extract($_GET);

    // On stock dans la variable url, l'url vers lequel on va faire la requête api.
    $url = "movie/" . $id_movie;

    // On stock dans la variable params le tableau de paramètre
    $params = [
        "language" => "fr"
    ]; 

    $movie = new Movie($url, $params);
}


?>

<!DOCTYPE html>

<html>

    <head>
        <meta charset="utf-8"/>
        <title>TMDB FILM</title>
        <link href="../style.css" rel="stylesheet">
    </head>

    <body>
        <h1 id="title">TMDB FILM</h1>
        
        <!-- Formulaire permettant de savoir le film souhaité par l'utilisateur grâce à l'id qu'il va fournir -->
        <form action="exploration-web.php" method="get">
            <label for="id_movie">id du film :</label>
            <input type="text" name="id_movie" id="id_movie" />
            <input type="submit" value="envoie">
        </form>

        <!-- Affichage en html des données récupérer à partir de l'api, si une requête à déjà était effectué -->
        <?php 
        
            if($movie != null) {
                echo "<p><strong>titre</strong>: " . $movie->getTitle() . "</p>";
                echo "<p><strong>titre original</strong>: " . $movie->getOriginalTitle() . "</p>";
                echo "<p><strong>tagline</strong>: " . $movie->getTagline() . "</p>";
                echo "<p><strong>description</strong>: " . $movie->getOverview() . "</p>";
                echo "<p><a href='". $movie->getLink() ."' target='_blank'>lien TMDB</a></p>";
            }
        
        ?>

    </body>

</html>