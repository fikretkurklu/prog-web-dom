<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("../Class/Movie.php");

// On stock par défaut null
$movies = null;

// On vérifie que les valeurs nécessaires sont contenu dans le tableau $_GET 
if (isset($_GET["id_movie"]) && !empty($_GET["id_movie"])) {

    extract($_GET);
    // On stock dans la variable url, l'url vers lequel on va faire la requête api, recevoir les données du film.
    $url = "movie/" . $id_movie;
    // On stock dans la variable url, l'url vers lequel on va faire la requête api, recevoir les données de l'équipe.
    $crewurl = "movie/$id_movie/credits";
    // On stock dans la variable url, l'url vers lequel on va faire la requête api, recevoir la bande annonce du film.
    $videourl = "movie/$id_movie/videos";

    // Lien vers la page tmdb du film
    $link = "https://www.themoviedb.org/movie/" . $id_movie;

    // On stock dans la variable params le tableau de paramètre
    $params = ["language" => "fr"];
    // On stock dans movie["fr"] les données récupérer en réalisant une requête api à l'url fourni, dans la langue souhaité, et décoder le json sous forme de tableau, avec lequel on créer un nouveau Movie
    $movies["fr"] = Movie::loadByApi($url, $params);
    // On stock dans la variable params le tableau de paramètre
    $params = ["language" => "en"];
    // On stock dans movie["en"] les données récupérer en réalisant une requête api à l'url fourni, dans la langue souhaité, et décoder le json sous forme de tableau, avec lequel on créer un nouveau Movie
    $movies["en"] = Movie::loadByApi($url, $params);
    // On stock dans movie["fr"] les données récupérer en réalisant une requête api à l'url fourni, dans la langue original, et décoder le json sous forme de tableau, avec lequel on créer un nouveau Movie
    $params = null;
    $movies["or"] = Movie::loadByApi($url, $params);

    // On stock dans crew, les données récupérer en réalisant une requête à l'url permettant d'avoir l'équipe du film, json décoder sous forme de tableau
    // $crew = json_decode(tmdbget($crewurl), true);

    // On stock dans video le lien vers la vidéo, récupérer à traver l'api.
    $video = "https://www.youtube.com/embed/" . json_decode(tmdbget($videourl), true)["results"][0]["key"];
}

?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8" />
    <title>TMDB FILM</title>
    <link href="../style.css" rel="stylesheet">
</head>

<body>
    <h1 id="title">TMDB FILM</h1>

    <!-- Formulaire permettant de savoir le film souhaité par l'utilisateur grâce à l'id qu'il va fournir -->
    <form action="movie.php" method="get">
        <label for="id_movie">id du film :</label>
        <input type="text" name="id_movie" id="id_movie" />
        <input type="submit" value="envoie">
    </form>

    <!-- On affiche sous forme de tableau les données récupérer -->
    <?php

    if ($movies != null) {
        echo "<p align='center'></div><iframe id='video' width='700' height='500' src='$video'></iframe></p>";
    ?>


        <table>

            <thead>

                <tr>
                    <th>Data</th>
                    <th>FR</th>
                    <th>EN</th>
                    <th>VO</th>
                </tr>

            </thead>

            <tbody>

                <tr>
                    <td>Poster</td>
                    <?php
                    foreach ($movies as $data) {
                        echo "<td class='poster'><img src='" . $data->getPosterPath() . "'></td>";
                    }
                    ?>
                </tr>

                <tr>
                    <td>Title</td>
                    <?php
                    foreach ($movies as $data) {
                        echo "<td>" . $data->getTitle() . "</td>";
                    }
                    ?>
                </tr>

                <tr>
                    <td>Original Title</td>
                    <?php
                    foreach ($movies as $data) {

                        echo "<td>" . $data->getOriginalTitle() . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Description</td>
                    <?php

                    foreach ($movies as $data) {
                        echo "<td>" . $data->getOverview() . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Tagline</td>
                    <?php

                    foreach ($movies as $data) {
                        echo "<td>" . $data->getTagline() . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Lien</td>

                    <?php

                    foreach ($movies as $data) {
                        echo "<td><a href='" . $data->getLink() . "' target='_blank'>lien TMDB</a></td>";
                    }

                    ?>
                </tr>

            </tbody>


            <table>

                <!-- On affiche le casting du film -->
            <?php

            echo "<div id='data'>";
            foreach ($movies["or"]->getActorList() as $person) {
                echo "<div class='dataElement'>";
                echo "<img src='https://image.tmdb.org/t/p/w200/" . $person->getProfilePath() . "' />";
                echo "<p><a target='_blank' href='actor_movie.php?name=" . $person->getName() . "&id=" . $person->getId() . "'>Nom acteur: " . $person->getName() . "</a></p>";
                echo "<p>Personnage joué: " . $person->getCharacterName() . "</p>";
                echo "<p>Nombre de film: " . $person->getNumberOfMovie() . " </p>";
                echo "</div>";
            }
            echo "</div>";
        }

            ?>

</body>

</html>