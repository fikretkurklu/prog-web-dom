<?php

require_once("../tp3-helpers.php");

// On stock par défaut null
$collection = null;

// On vérifie que les valeurs nécessaires sont contenu dans le tableau $_GET 
if (isset($_GET["id_movie"]) && !empty($_GET["id_movie"])) {

    extract($_GET);

    // On stock dans la variable url, l'url vers lequel on va faire la requête api, recevoir les données de la collection.
    $url = "collection/" . $id_movie;

    // On stock sous forme de tableau, les données de collection, décodé
    $collection = json_decode(tmdbget($url), true);
}


?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8" />
    <title>TMDB COLLECTION</title>
    <link href="../style.css" rel="stylesheet">
</head>

<body>

    <h1 id="title">TMDB COLLECTION</h1>

    <form action="collection.php" method="get">
        <label for="id_movie">id du film :</label>
        <input type="text" name="id_movie" id="id_movie" />
        <input type="submit" value="envoie">
    </form>

    <?php

    if ($collection != null) {

    ?>
        <table>

            <thead>

                <tr>
                    <th>Title</th>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<th>" . $data["title"] . "</th>";
                    }

                    ?>
                </tr>

            </thead>

            <tbody>

                <tr>

                    <td>Poster</td>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<td class='poster'><img src='https://image.tmdb.org/t/p/w300" . $data["poster_path"] . "'/></td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Original Title</td>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<td>" . $data["original_title"] . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>id</td>
                    <?php
                    foreach ($collection["parts"] as $data) {
                        echo "<td>" . $data["id"] . "</td>";
                    }
                    ?>
                </tr>

                <tr>
                    <td>Date de sortie</td>
                    <?php
                    foreach ($collection["parts"] as $data) {
                        echo "<td>" . $data["release_date"] . "</td>";
                    }
                    ?>
                </tr>

                <tr>
                    <td>Description</td>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<td>" . $data["overview"] . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Tagline</td>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<td>" . $data["tagline"] . "</td>";
                    }

                    ?>
                </tr>

                <tr>
                    <td>Lien</td>
                    <?php

                    foreach ($collection["parts"] as $data) {
                        echo "<td><a href='https://www.themoviedb.org/collection/" . $data["id"] . "' target='_blank'>lien TMDB</a></td>";
                    }

                    ?>
                </tr>

            </tbody>


            <table>

            <?php

        }

            ?>

</body>

</html>