<?php
require_once("../tp3-helpers.php");
require_once("../Class/Actor.php");

// On stock par défaut null
$id = null;

// On vérifie que les valeurs nécessaires sont contenu dans le tableau $_GET
if (isset($_GET["id"]) && isset($_GET["name"])) {
    if (!empty($_GET["id"]) && !empty($_GET["name"])) {
        extract($_GET);

        // On créer un nouvel Actor avec l'id et le name fournit à traver la requête get 
        $actor = new Actor(
            array(
                "id" => $id,
                "name" => $name,
                "character" => NULL,
                "profile_path" => NULL
            )
        );
        // On stock dans la variable url, l'url vers lequel on va faire la requête api, recevoir les données des films joué par l'acteur.
        $url = "person/" . $actor->getId() . "/movie_credits";
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>TMDB Movie or Collection</title>
    <link rel="stylesheet" href="../style.css">
</head>

<body>
    <!-- On affiche les films jouée par l'acteur -->
    <?php if (!isset(json_decode(tmdbget($url), true)["status_code"])) { ?>
        <h1 id="title">Film joué par : <?php echo $actor->getName(); ?></h1>
        <div id='data'>
        <?php
        foreach ($actor->getMovieList() as $movie) {
            echo "<div class='dataElement'>";
            echo "<td><img src='" . $movie->getPosterPath() . "'></td>";
            echo "<p>Titre: " . $movie->getTitle() . "</p>";
            echo "<p>Rôle: " . $movie->getCharacter() . "</p>";
            echo "</div>";
        }
    }
        ?>
</body>

</html>