<?php

require_once("Movie.php");

class Actor
{

    private $_id;
    private $_name;
    private $_characterName;
    private $_movieList;
    private $_profilePath;

    public function __construct($actorData)
    {
        $this->setId($actorData["id"]);
        $this->setName($actorData["name"]);
        $this->setCharacterName($actorData["character"]);
        $this->setProfilePath($actorData["profile_path"]);
        $this->setMovieList();
    }

    /**
     * We set the id of the actor
     * @param int $id id of the actor
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * We set the name of the actor
     * @param string $name name of the actor
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * We set the name of the actor in the movie
     * @param string $characterName name of the actor in the movie
     */
    public function setCharacterName($characterName)
    {
        $this->_characterName = $characterName;
    }

    /**
     * We set the movie list by making a request to the tmdb api
     */
    public function setMovieList()
    {
        $apiResult = json_decode(tmdbget("person/" . $this->getId() . "/movie_credits"), true);
        $this->_movieList = array();
        foreach ($apiResult["cast"] as $movieData) {
            array_push($this->_movieList, Movie::loadByArray($movieData));
        }
    }

    /**
     * We set the profile path of the actor
     * @param string $profilePath path to the tmdb page of the actor
     */
    public function setProfilePath($profilePath)
    {
        $this->_profilePath = $profilePath;
    }

    /**
     * We get the id of the actor
     * @return int id of the actor
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * We get the name of the actor
     * @return string name of the actor
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * We get the name of the actor in the movie
     * @return string character name of the actor
     */
    public function getCharacterName()
    {
        return $this->_characterName;
    }

    /**
     * We get the number of movie played by the actor
     * @return int number of movie
     */
    public function getNumberOfMovie()
    {
        return sizeof($this->_movieList);
    }

    /**
     * We get the movie list played by the actor
     * @return mixed movie list
     */
    public function getMovieList()
    {
        return $this->_movieList;
    }

    /**
     * We get the profile path of the actor
     * @return string profile path
     */
    public function getProfilePath()
    {
        return $this->_profilePath;
    }
}
