<?php

require_once("../tp3-helpers.php");
require_once("Actor.php");

class Movie
{

    private $_id;
    private $_title;
    private $_originalTitle;
    private $_overview;
    private $_tagline;
    private $_link;
    private $_posterPath;
    private $_actorList;
    private $_character;

    public function __construct()
    {
    }

    /**
     * We set the data of Movie by making a call to the api
     * @param string $url url where we will make the api call
     * @param mixed $params array with the params for the api call
     */
    public static function loadByApi($url, $params)
    {
        $instance = new self();
        $result = tmdbget($url, $params);
        $movie = json_decode($result, true);
        $instance->setId($movie["id"]);
        $instance->setOriginalTitle($movie["original_title"]);
        $instance->setTitle($movie["title"]);
        $instance->setTagline($movie["tagline"]);
        $instance->setOverview($movie["overview"]);
        $instance->setLink("https://www.themoviedb.org/movie/" . $movie["id"]);
        $instance->setPosterPath("https://image.tmdb.org/t/p/w300" . $movie["poster_path"]);

        $crew = json_decode(tmdbget($url . "/credits"), true);
        $instance->_actorList = array();
        foreach ($crew["cast"] as $actorData) {
            array_push($instance->_actorList, new Actor($actorData));
        }

        return $instance;
    }

    /**
     * We set the data of Movie by using an array of data
     * @param mixed $movieData array with data for movie
     */
    public static function loadByArray($movieData)
    {
        $instance = new self();

        $instance->setId($movieData["id"]);
        $instance->setOriginalTitle($movieData["original_title"]);
        $instance->setTitle($movieData["title"]);
        $instance->setOverview($movieData["overview"]);
        $instance->setLink("https://www.themoviedb.org/movie/" . $movieData["id"]);
        $instance->setPosterPath("https://image.tmdb.org/t/p/w300" . $movieData["poster_path"]);
        $instance->setCharacter($movieData["character"]);

        return $instance;
    }

    /**
     * We set the id of the movie
     * @param int $id id of the movie
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * We set the title of the movie
     * @param string $title title of the movie
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }

    /**
     * We set the original title of the movie
     * @param string $originalTitle original title of the movie
     */
    public function setOriginalTitle($originalTitle)
    {
        $this->_originalTitle = $originalTitle;
    }

    /**
     * We set the overview of the movie
     * @param string $overview overview of the movie
     */
    public function setOverview($overview)
    {
        $this->_overview = $overview;
    }

    /**
     * We set the tagline of the movie
     * @param string $tagline tagline of the movie
     */
    public function setTagline($tagline)
    {
        $this->_tagline = $tagline;
    }

    /**
     * We set the link of the movie
     * @param string $link link to the tmdb page of the movie
     */
    public function setLink($link)
    {
        $this->_link = $link;
    }

    /**
     * We set the poster path of the movie
     * @param string $posterPath path to the poster of the movie
     */
    public function setPosterPath($posterPath)
    {
        $this->_posterPath = $posterPath;
    }

    /**
     * We set the character of the movie
     * @param string $title title of the movie
     */
    public function setCharacter($character)
    {
        $this->_character = $character;
    }

    /**
     * We get the id of the movie
     * @return int id of the movie
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * We get the title of the movie
     * @return string title of the movie
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * We get the original title of the movie
     * @return string original title of the movie
     */
    public function getOriginalTitle()
    {
        return $this->_originalTitle;
    }

    /**
     * We get the overview of the movie
     * @return string overview of the movie
     */
    public function getOverview()
    {
        return $this->_overview;
    }

    /**
     * We get the tagline of the movie
     * @return string tagline of the movie
     */
    public function getTagline()
    {
        return $this->_tagline;
    }

    /**
     * We get the link of the movie
     * @return string link of the movie
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * We get the poster path of the movie
     * @return string poster path of the movie
     */
    public function getPosterPath()
    {
        return $this->_posterPath;
    }

    /**
     * We get the actor list of the movie
     * @return mixed actor list of the movie
     */
    public function getActorList()
    {
        return $this->_actorList;
    }

    /**
     * We get the character of the movie
     * @return string character of the movie
     */
    public function getCharacter()
    {
        return $this->_character;
    }
}
