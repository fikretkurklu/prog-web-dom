# Compte-rendu de TP 3

## Participants

- Fikret KÜRKLÜ

- Valentin DE OLIVEIRA

- Mahaman Noury ABDOURAHAMANE

## TP

### Mise en jambes

1.  **Exploration** : Le format de réponse est le JSON, il s'agit du film Fight Club. En incluant le paramètre, on obtient un réponse contenant des données en français

2.  **Page de détail**: Le code est contenu dans le fichier exploration-web.php

### Les choses sérieuses

3.  **Page en 3 colonnes (web)** : Le code est contenu dans le fichier movie.php

4.  **Requête (web)**: Le code est contenue dans le fichier collection.php

5.  **Distribution (web)**: Le code est contenue dans le fichier movie.php

6.  **Casting**: Le code est contenue dans le fichier movie.php et actor_movie.php, c'est simple à réaliser, nous avons réaliser une page actor_movie.php dans lequel nous envoyons les données id et name de l'acteur. Avec ses données nous faisons une requête à l'api au lien person/$id/movie_credits à travers lequel nous récupérons les films joué par l'acteur en accédant au donnée avec la clé cast du tableau récupérer.

7.  **Bande-annonce**.: Le code est contenue dans le fichier movie.php
