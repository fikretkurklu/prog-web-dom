<?php
    if(isset($_GET["col"]) && isset($_GET["row"])){
        extract($_GET);
    }
    else{
        $col = 10;
        $row = 10;
    }
?>
<html>

<head>
    <title>multiplication</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    
    <style>
        .surlignee{
            background-color: #FF00FF;
        }
    </style>
</head>

<body>
    <table method="get" action="action.php">
        <thead>
            <tr>
                <?php
                for($i = 1 ; $i <= $col ; $i++){
                    echo "<th>$i</th>";
                }
                ?>
            </tr>
        </thead>
        <?php
            for($i = 1 ; $i <= $row ; $i++){
                if($i == $row/2){
                    echo "<tr class='surlignee'> ";
                }  
                else{
                    echo "<tr>";                    
                }             


                for($j = 1 ; $j <= $col ; $j++){
                    echo "<td>" . $i * $j . "</td>";
                }
                echo "</tr>";
            }
        ?>
    </table>
</body>

</html>