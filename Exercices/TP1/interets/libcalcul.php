<?php
function cumul($somme, $taux, $duree) {
    $cumul = $somme * pow( 1 + $taux/100, $duree);
    return $cumul;
}