% PW-DOM Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : ---

## Participants

- Mahaman Noury Abdourahamane
- Fikret Kürklü
- Valentin De Oliveira

## Avis sur les TP

#### Calcul d’intérêts composés

On a fait passer les informations du formulaire par **POST**, quand l'utilisateur a rempli les champs pour le calcul d'intérêts, on va appeler le fichier resultat.php qui va déléguer le calcul à une fonction _cumul_ à l'intérieur du fichier libcalcul.php

#### Un peu de style en CSS

Encore une fois pas de difficultés particulières, on inclut le fichier CSS dans notre HTML et on y définit le style voulu à l'intérieur de ce dernier

#### Table de multiplication

On a dû cette fois-ci faire preuve d'un peu plus de technique, pour cela, avant d'arriver au code HTML on vérifie simplement si les variables **col** et **row** sont passées par l'URL, si elle ne le sont pas on les initialise à 10, l'affichage de la table est réalisé avec une boucle for. La ligne surlignée peut-être choisi en modifiant le 1er if du for, dans le cas présent la ligne surlignée est la ligne du milieu.

#### Analyse des Caractères Unicode

Cette partie nous a permis de mettre en commun tout ce qui a été fait précédemment on doit tester si la variable **phrase** est passée par l'URL si elle est définie on peut directement afficher le premier caractère unicode de chacun des mots, dans tous les cas l'utilisateur peut saisir sa phrase à l'aide d'un formulaire pour aboutir au même résultat.

#### Calendrier - agenda web

La véritable difficulté venait de la compréhension des arguments que la fonction _date_ peut prendre et d'utiliser la fonction _strtotime_ pour convertir une date en un timestamp utilisable par la fonction _date_, une fois cela comprit et maîtrisé, le développement s'est déroulé sans encombre.

#### Analyse (parsing) d’un document HTML
