<?php

    require_once("lib_unicode.php");

    if(isset($_GET["phrase"])) {
        extract($_GET);
    } else {
        $phrase = NULL;
    }

?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <title>web unicode</title>
    </head>
    <body>
    
    <form method="get" action="web_unicode.php">
        <label for="phrase">phrase</label> <input type="text" id="phrase" name="phrase"/> <br/>
        <input type="submit"/>
    </form>

    <?php
        if($phrase != NULL) {
            $phrase_array = explode(' ', $phrase);

            for($i = 0; $i < count($phrase_array); $i++) {
                echo '<p>' . to_utf8($phrase_array[$i]) . '</p>';
            }
        }
    ?>
    
    </body>
</html>
