<?php

$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML(file_get_contents($argv[1]));
libxml_use_internal_errors(false);
$titles = $dom->getElementsByTagName("h2");

foreach ($titles as $title) {
    echo $title->textContent;
}