<?php
    if(isset($_POST["jours"]) && isset($_POST["evenement"])) {
        extract($_POST);
    } else {
        $jours = -1;
        $evenement = "nope";
    }

    if(isset($_GET["mois"])) {
        $mois = date('F', mktime(0, 0, 0, $_GET["mois"], 10));
    } else {
        $mois = date("F");
    }

    if(isset($_GET["annee"])){
        $annee = $_GET["annee"];
    }
    else{
        $annee = date("Y");
    }
    $nb_day = date("t", strtotime($mois . ' ' . $annee));

    $currentStartDate = 0;

    switch(date("l", strtotime("1 " . $mois . " " . $annee))) {
        case("Monday"):
            $currentStartDate = 1;
        break;
        case("Tuesday"):
            $currentStartDate = 2;
        break;
        case("Wednesday"):
            $currentStartDate = 3;
        break;
        case("Thursday"):
            $currentStartDate = 4;
        break;
        case("Friday"):
            $currentStartDate = 5;
        break;
        case("Saturday"):
            $currentStartDate = 6;
        break;
        case("Sunday"):
            $currentStartDate = 7;
        break;
    }
?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <title>date web</title>
    </head>
    
    <body>

        <form method="post" action="agendaTabulaire.php">
            <label for="jours">jours</label> <input type="text" id="jours" name="jours" /> <br />
            <label for="evenement">evenement</label> <input type="text" id="evenement" name="evenement" />
        <input type="submit" />
        </form>
    
        <table>

            <thead>
                <tr>
                    <th scope="col">Lundi</th>
                    <th scope="col">Mardi</th>
                    <th scope="col">Mercredi</th>
                    <th scope="col">Jeudi</th>
                    <th scope="col">Vendredi</th>
                    <th scope="col">Samedi</th>
                    <th scope="col">Dimanche</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                    $i = 1;
                    $currentDay = 1;
                    $firstRow = true;

                    while($i <= $nb_day) {
                        if($currentDay == 1) {
                            echo "<tr>";
                        }

                        $tableContent = $i;
                        if($jours != -1) {
                            if($jours == $i) {
                                $tableContent = $i . "<br>". $evenement;
                            }
                        }

                        if($firstRow) {
                            if($currentDay == $currentStartDate) {
                                echo "<td>" . $tableContent . "</td>";
                                $firstRow = false;
                                $i++;
                            } else {
                                echo "<td></td>";
                            }
                        } else {
                            echo "<td>" . $tableContent . "</td>";
                            $i++;
                        }

                        

                        $currentDay++;
                        if($currentDay == 8) {
                            $currentDay = 1;
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>

        </table>
    
    </body>
</html>