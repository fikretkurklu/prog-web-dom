# Compte-rendu de TP 2

## Participants

- Fikret KÜRKLÜ

- Valentin DE OLIVEIRA

- Mahaman Noury ABDOURAHAMANE

## TP

### Filtres Unix

0.  **Visualisation texte** : `cat borneswifi_EPSG4326_20171004_utf8.csv | wc -l` Donne le nombre de ligne du fichier, donne 68
1.  **Comptage** : `cut -d "," -f 2 borneswifi_EPSG4326_20171004_utf8.csv | uniq | wc -l` Le nombre de point d'accès est 58
2.  **Points multiples** : `cut -d "," -f 2 borneswifi_EPSG4326_20171004_utf8.csv | uniq -c | sort | tail -n 1` L’endroit possédant le plus de point d’accès est la Bibliothèque Etudes avec 5 points d’accès.

### Traitements PHP

3. **Comptage PHP** : Pour réaliser le comptage en php, nous avons tout d'abord ouvert le fichier et stocker son contenue sous forme de tableau dans la variable borne_wifi : `$borne_wifi = file("../data/borneswifi_utf8.csv");`. Nous avons par la suite compter le nombre d'élement dans le tableau borne_wifi, en utilisant la fonction php count : `$nombre_element = count($borne_wifi);`, ce qui nous donne 68. _Le code est contenue dans le fichier traitements php/question345.php_
4. **Structure de données**: Pour mettre en forme les données récupéré dans la question précedente, nous avons appellé une boucle foreach sur le tableau borne_wifi, pour chaque élement du tableau, nous avons appellé la fonction fournit par le professeur qui met en forme les données passée en paramètre, les données mis en forme sont par la suite stocké dans un nouveau tableau bornes_wifi : ` array_push($bornes_wifi,initAccesspoint(str_getcsv($line)));`
   _Le code est contenue dans le fichier traitements php/question345.php_
5. **Proximité**: Il y'a 7 bornes à moins de 200m de la place Grenette. La borne la plus proche est la borne Place Grenette, il se trouve à 20,1 mètres. _Le code est contenue dans le fichier traitements php/question345.php_
6. **Proximité top N**: La valeur de N doit être fourni en paramètre lors de l'appelle en ligne de comamnde _Le code est contenue dans le fichier traitements php/question6.php_
7. **Géocodage**: Pour réaliser l'appelle à l'api, nous utilisons la fonction smartcurl fourni par le professeur _Le code est contenue dans le fichier traitements php/question7.php_
8. **Webservice**: _Le code est contenue dans le fichier traitements php/question8.php_
9. **Format json**: La différence avec la question est précedente est l'ouverture du fichier et son intérprétation qui est fait de façon différente: _Le code est contenue dans le fichier traitements php/question9.php_

```php
	$borne_wifi =  file_get_contents("../data/borneswifi_EPSG4326_20171004.json");

	$borne_wifi =  json_decode($borne_wifi,  true);
```

10. **Client webservice**: _Le code est contenue dans le fichier traitements php/question10.php_

### Antennes GSM

1. **CSV Antennes**: 100 antennes sont référencées dans le fichier, il y a plusieurs données en plus tel que l'opérateur de l'antenne (sfr, orange, ...), si l'antenne est une antenne 2G, 3G ou bien 4G, les technos qu'elle comporte etc... Dans le cadre d'une démarche Opendata, cela favorise l'accès à ses données, à leur réutilisation dans de futur carte par exemple permettant de savoir la localisation des différentes antennes.
2. **Statistiques opérateurs**: Il y'a 4 opérateurs dans ce jeu de données, ils ont chacun un certain nombre d'antennes (Bouygues -> 26, Free -> 18, Orange -> 26, SFR -> 30). Pour répondre à ses questions, j'ai utilisé le shell, je trouve plus simple et rapide à manipuler des fichiers .csv avec et d'avoir le résultat affiché, sans avoir à créer de fichier à part. Les commandes que j'ai utilisées sont les suivantes : `cut -d ";" -f 4 DSPE_ANT_GSM_EPSG4326.csv | sort | uniq` et `cut -d ";" -f 4 DSPE_ANT_GSM_EPSG4326.csv | sort | uniq -c`
3. **KML validation**
4. **KML bis**
5. **Top N opérateur**: _Le code est contenue dans le fichier gsm/question5.php_
