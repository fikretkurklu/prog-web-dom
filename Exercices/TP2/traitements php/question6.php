<?php

require_once("../elem_function.php");

echo "-----------------QUESTION 6 : \n";

$borne_wifi = file("../data/borneswifi_utf8.csv");
$bornes_wifi = array();

foreach($borne_wifi as $line){
    array_push($bornes_wifi,initAccesspoint(str_getcsv($line)));
}

// Résidence Berlioz, résidence où on habite
$ma_position = array("lat" => 45.189493, "lon" => 5.759456);

$distances = array();

foreach($bornes_wifi as $borne) {
    $distance = distance($borne, $ma_position);

    array_push($distances, array("name" => $borne["name"], "distance" => $distance));
}

echo "Top 5 des points d'accès les plus proches \n";

$dist = array_column($distances, "distance");

array_multisort($dist, SORT_ASC, $distances);

for($i = 0 ; $i < $argv[1]; ++$i){
    echo ($i+1) ." : ". $distances[$i]["name"] . "\n";
} 

?>