<?php
    require_once("../elem_function.php");


    if(isset($_GET["lat"]) && isset($_GET["lon"]) && isset($_GET["top"])){
        extract($_GET);
    }
    else{
        $lat = 45.19102;
        $lon = 5.72752;
        $top = 5;
    }

    header('Content-type: application/json');
    echo get_top_distance_csv_to_json($top, $lon, $lat);
