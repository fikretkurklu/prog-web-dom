<?php

require_once("../elem_function.php");

echo "-----------------QUESTION 3-4 : \n";

$borne_wifi = file("../data/borneswifi_utf8.csv");
$bornes_wifi = array();

foreach($borne_wifi as $line){
    array_push($bornes_wifi,initAccesspoint(str_getcsv($line)));
}

$nombre_element = count($borne_wifi);

echo "Il y'a $nombre_element borne wifi au total\n";

echo "-----------------QUESTION 5 : \n";

$grenet = array("lat" => 45.19102, "lon" => 5.72752);

$distances = array();
$min = 1000.0;
$min_name = "";

echo "Les bornes wifi à moins de 200 m : \n";

foreach($bornes_wifi as $borne) {
    $distance = distance($borne, $grenet);

    if($min > $distance) {
        $min_name = $borne["name"];
        $min = $distance;
    }

    if($distance < 200) 
        echo "nom: " . $borne['name'] . " | distance: $distance \n";

    array_push($distances, array("name" => $borne["name"], "distance" => $distance));
}

echo "La borne wifi la plus proche est la borne $min_name, et il se trouve à $min \n";


?>