<?php

require_once("../elem_function.php");

echo "-----------------QUESTION 7 : \n";

$borne_wifi = file("../data/borneswifi_utf8.csv");
$bornes_wifi = array();

foreach($borne_wifi as $line){
    array_push($bornes_wifi,initAccesspoint(str_getcsv($line)));
}

// Résidence Berlioz, résidence où on habite
for($i = 0; $i < count($bornes_wifi); $i++) {
    $borne = $bornes_wifi[$i];
    $bornes_wifi[$i]["adr_comp"] = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse?lon=". $borne["lon"] ."&lat=" . $borne["lat"], 0), true)["features"][0]["properties"]["label"];
    print_r($bornes_wifi[$i]);
}

echo "L'adresse complète est affiché dans l'entrée adr_comp de chaque tableau !\n";

?>