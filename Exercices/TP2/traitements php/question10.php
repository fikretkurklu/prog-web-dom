<?php
    require_once("../elem_function.php");


    if(isset($_GET["lat"]) && isset($_GET["lon"]) && isset($_GET["top"])){
        extract($_GET);
    }
    else{
        $lat = 45.19102;
        $lon = 5.72752;
        $top = 5;
    }

?>

<!DOCTYPE html>


<html>


<head>
    <meta charset="utf-8">
    <title>web service</title>
</head>

<body>
    <form method="get" action="question10.php">
        <label for="lon">lon</label> <input type="text" id="lon" name="lon"/> <br />
        <label for="lat">lat</label> <input type="text" id="lat" name="lat"/> <br />
        <label for="top">top</label> <input type="text" id="top" name="top"/>
        <input type="submit" />
    </form>

    <?php get_top_distance_json_to_array($top, $lon, $lat); ?>
</body>

</html>