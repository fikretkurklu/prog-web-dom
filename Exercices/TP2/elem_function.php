<?php

function initAccesspoint($row) {
    return array(
        'name' => $row[0], //string
        'adr' => $row[1],  //string
        'lon' => $row[2],  //float, in decimal degrees
        'lat' => $row[3]  //float, in decimal degrees
    );  
}

function initAccesspointJsonOperateur($row) {
    return array(
        'name' => $row["properties"]["ANT_ADRES_LIBEL"], //string
        'operateur' => $row["properties"]["OPERATEUR"],  //string
        'lon' => $row["geometry"]["coordinates"][0],  //float, in decimal degrees
        'lat' => $row["geometry"]["coordinates"][1]   //float, in decimal degrees
    );  
}

function initAccesspointJson($row) {
    return array(
        'name' => $row["properties"]["AP_ANTENNE1"], //string
        'adr' => $row["properties"]["Antenne 1"],  //string
        'lon' => $row["properties"]["longitude"],  //float, in decimal degrees
        'lat' => $row["properties"]["latitude"]   //float, in decimal degrees
    );  
}

function distance ($p, $q) {
    $scale = 10000000 / 90; // longueur d'un degré le long d'un méridien
    $a = ((float)$p['lon'] - (float)$q['lon']);
    $b = (cos((float)$p['lat']/180.0*M_PI) * ((float)$p['lat'] - (float)$q['lat']));
    $res = $scale * sqrt( $a**2 + $b**2 );
    return (float)sprintf("%5.1f", $res);
}

function smartcurl($url, $verb) {
    $ch = curl_init();
	
	if ($verb == 2) { echo "$url\n"; }
    if ($verb == 1) { echo "." ; }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);
    curl_close($ch);      

    return $output;
}

function get_top_distance_csv_to_json($top, $lon, $lat) {

    $borne_wifi = file("../data/borneswifi_utf8.csv");
    $bornes_wifi=array();

    foreach($borne_wifi as $line){
        array_push($bornes_wifi, initAccesspoint(str_getcsv($line)));
    }
    $place_array = array("lat" => $lat, "lon" => $lon);

    $distances = array();

    foreach($bornes_wifi as $borne) {
        $distance = distance($borne, $place_array);
        array_push($distances, array("name" => $borne["name"], "distance" => $distance));
    }

    $dist = array_column($distances, "distance");

    array_multisort($dist, SORT_ASC, $distances);

    $json_response = array();

    for($i = 1 ; $i < $top + 1; ++$i){
        array_push($json_response, $distances[$i]);
    } 

    return json_encode($json_response);
}

function get_top_distance_json_to_json($top, $lon, $lat) {

    $borne_wifi = file_get_contents("../data/borneswifi_EPSG4326_20171004.json");
    $borne_wifi = json_decode($borne_wifi, true);
    $bornes_wifi=array();

    foreach($borne_wifi["features"] as $line) {
        array_push($bornes_wifi, initAccesspointJson($line));
    }
    $place_array = array("lat" => $lat, "lon" => $lon);

    $distances = array();

    foreach($bornes_wifi as $borne) {
        $distance = distance($borne, $place_array);
        array_push($distances, array("name" => $borne["name"], "distance" => $distance));
    }

    $dist = array_column($distances, "distance");

    array_multisort($dist, SORT_ASC, $distances);

    $json_response = array();

    for($i = 1 ; $i < $top + 1; ++$i){
        array_push($json_response, $distances[$i]);
    } 

    return json_encode($json_response);
}

function get_top_distance_json_to_array($top, $lon, $lat) {

    $borne_wifi = file_get_contents("../data/borneswifi_EPSG4326_20171004.json");
    $borne_wifi = json_decode($borne_wifi, true);
    $bornes_wifi=array();

    foreach($borne_wifi["features"] as $line) {
        array_push($bornes_wifi, initAccesspointJson($line));
    }
    $place_array = array("lat" => $lat, "lon" => $lon);

    $distances = array();

    foreach($bornes_wifi as $borne) {
        $distance = distance($borne, $place_array);
        array_push($distances, array("name" => $borne["name"], "distance" => $distance));
    }

    $dist = array_column($distances, "distance");

    array_multisort($dist, SORT_ASC, $distances);

    echo "<table><thead><tr><th>number</th><th>name</th></tr></thead><tbody>";
    for($i = 1 ; $i < $top + 1; ++$i){
        echo "<tr>";
        echo "<td>" . $i ." </td><td>". $distances[$i]["name"] . "</td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}

function get_top_distance_json_to_array_gsm($top, $lon, $lat, $operateur) {
    $borne_wifi = file_get_contents("../data/DSPE_ANT_GSM_EPSG4326.json");
    $borne_wifi = json_decode($borne_wifi, true);
    $bornes_wifi=array();

    foreach($borne_wifi["features"] as $line) {
        array_push($bornes_wifi, initAccesspointJsonOperateur($line));
    }

    $place_array = array("lat" => $lat, "lon" => $lon);

    $distances = array();

    foreach($bornes_wifi as $borne) {
        $distance = distance($borne, $place_array);
        if($borne["operateur"] == $operateur)
            array_push($distances, array("name" => $borne["name"], "distance" => $distance));
    }

    $dist = array_column($distances, "distance");

    array_multisort($dist, SORT_ASC, $distances);

    echo "<table><thead><tr><th>number</th><th>name</th></tr></thead><tbody>";
    for($i = 1 ; $i < $top + 1; ++$i){
        echo "<tr>";
        echo "<td>" . $i ." </td><td>". $distances[$i]["name"] . "</td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}