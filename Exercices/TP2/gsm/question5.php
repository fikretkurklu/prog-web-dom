<?php
    require_once("../elem_function.php");


    if(isset($_GET["operateur"]) && isset($_GET["lat"]) && isset($_GET["lon"]) && isset($_GET["top"])){
        extract($_GET);
    }
    else{
        $operateur = "SFR";
        $lat = 45.19102;
        $lon = 5.72752;
        $top = 5;
    }

?>

<!DOCTYPE html>


<html>


<head>
    <meta charset="utf-8">
    <title>web service</title>
</head>

<body>
    <form method="get" action="question5.php">
        <label for="operateur">operateur</label> 
            <select name="operateur" id="operateur">
                <option value="">--Please choose an option--</option>
                <option value="SFR">SFR</option>
                <option value="FREE">Free</option>
                <option value="BYG">Bouygues</option>
                <option value="ORA">Orange</option>
            </select> <br />
        <label for="lon">lon</label> <input type="text" id="lon" name="lon"/> <br />
        <label for="lat">lat</label> <input type="text" id="lat" name="lat"/> <br />
        <label for="top">top</label> <input type="text" id="top" name="top"/>
        <input type="submit" />
    </form>

    <?php get_top_distance_json_to_array_gsm($top, $lon, $lat, $operateur); ?>
</body>

</html>